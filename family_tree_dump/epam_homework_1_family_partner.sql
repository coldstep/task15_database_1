-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: epam_homework_1
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `family_partner`
--

DROP TABLE IF EXISTS `family_partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `family_partner` (
  `family_partner_id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `day_of_birth` date NOT NULL,
  `birthday_place` varchar(45) NOT NULL,
  `death_place` varchar(45) DEFAULT NULL,
  `gender_id` int(11) NOT NULL,
  PRIMARY KEY (`family_partner_id`,`gender_id`),
  KEY `fk_family_partner_gender1_idx` (`gender_id`),
  CONSTRAINT `family_partner_id_family_tree_id` FOREIGN KEY (`family_partner_id`) REFERENCES `family_tree` (`family_tree_id`),
  CONSTRAINT `fk_family_partner_gender1` FOREIGN KEY (`gender_id`) REFERENCES `gender` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `family_partner`
--

LOCK TABLES `family_partner` WRITE;
/*!40000 ALTER TABLE `family_partner` DISABLE KEYS */;
INSERT INTO `family_partner` VALUES (2,'Scarlett','Yohanson','1988-02-23','Santa-Monica',NULL,2),(3,'Mila','Yovovich','1983-04-21','Ukraine',NULL,2),(4,'Jason','Stetham','1988-02-21','London',NULL,1),(5,'Emily','Portmen','1998-03-12','New-York',NULL,2),(6,'Charlize','Theron','1980-12-03','Africa',NULL,2);
/*!40000 ALTER TABLE `family_partner` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-20 14:16:51
