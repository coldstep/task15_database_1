-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: epam_homework_11
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `student` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `middle_name` varchar(45) NOT NULL,
  `general_rating` double DEFAULT NULL,
  `birthday` date NOT NULL,
  `data_of_entry` date DEFAULT NULL,
  `student_number` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `student_info` varchar(45) GENERATED ALWAYS AS (concat(`student_number`,_utf8mb4'-',`data_of_entry`)) VIRTUAL,
  `full_name` varchar(100) GENERATED ALWAYS AS (concat(`surname`,_utf8mb3' ',`name`,_utf8mb3' ',`middle_name`)) VIRTUAL,
  `city_city_id` int(11) NOT NULL,
  `secondary_education_id` int(11) NOT NULL,
  `group_group_id` int(11) NOT NULL,
  PRIMARY KEY (`student_id`),
  KEY `fk_student_city1_idx` (`city_city_id`),
  KEY `fk_student_secondary_education1_idx` (`secondary_education_id`),
  KEY `fk_student_group1_idx` (`group_group_id`),
  CONSTRAINT `fk_student_city1` FOREIGN KEY (`city_city_id`) REFERENCES `city` (`city_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_student_group1` FOREIGN KEY (`group_group_id`) REFERENCES `group` (`group_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_student_secondary_education1` FOREIGN KEY (`secondary_education_id`) REFERENCES `secondary_education` (`secondary_education_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` (`student_id`, `surname`, `name`, `middle_name`, `general_rating`, `birthday`, `data_of_entry`, `student_number`, `email`, `city_city_id`, `secondary_education_id`, `group_group_id`) VALUES (1,'Кличко','Віталій','Михайлович',3,'1970-02-21','1988-09-01',2352351,'klichko@gmail.com',1,1,2),(2,'Могила','Петро','Васильович',5,'1960-02-12','1978-09-01',2120092,'mogila@gmail.com',2,2,3),(3,'Тимошенко','Юлія','Володимирівна',5,'1980-03-21','1998-09-01',2139971,'timoshenko@gmail.com',3,3,1),(4,'Кардашян','Кім','Альбертівна',3,'1990-03-12','2008-09-01',1283290,'kimka@gmail.com',4,4,4),(5,'Янукович','Петро','Васильович',1,'1965-05-04','1990-02-12',1390281,'petya@gmail.com',5,5,5);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-20 15:07:43
