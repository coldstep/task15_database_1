1.  SELECT *
	FROM pc
    WHERE model RLIKE "1{2}";
    #Or
    SELECT *
	FROM pc
    WHERE model LIKE "%1%1%";
2.  SELECT *
	FROM outcome
    WHERE outcome.date LIKE "%-03-%";
3.  SELECT *
	FROM outcome_o
    WHERE outcome_o.date LIKE "%-14%";
4.  SELECT *
	FROM ships
    WHERE name LIKE "W%n";
5.  SELECT *
	FROM ships
    WHERE name LIKE "%e%e%";
6.  SELECT name, launched
	FROM ships
    WHERE name NOT LIKE "%a";  
7.  SELECT battle
	FROM outcomes
    WHERE battle Like "% %c";
8.  SELECT *
	FROM trip
    WHERE time_out RLIKE "[[:space:]][1][2-7]:[0-9][0-9]:[0-9][0-9]";
9.  SELECT *
	FROM trip
    WHERE (hour(time_in) between 17 and 23);
10. SELECT date
	FROM pass_in_trip
    WHERE place like "1_";
11. SELECT date
	FROM pass_in_trip
    WHERE place like "_c";
12. SELECT name
	FROM passenger
    WHERE name LIKE "% C%";
13. SELECT name
	FROM passenger
    WHERE name NOT LIKE "% J%";